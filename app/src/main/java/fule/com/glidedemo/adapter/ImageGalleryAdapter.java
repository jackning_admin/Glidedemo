package fule.com.glidedemo.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import fule.com.glidedemo.R;

/**
 * 作者： njb
 * 时间： 2018/10/19 0019-上午 11:02
 * 描述：
 * 来源：
 */
public class ImageGalleryAdapter extends BaseAdapter {
    private Context context;
    // 里面所有的方法表示的是可以根据指定的显示图片的数量,进行每个图片的处理
    private Integer[] images = { R.mipmap.image1, R.mipmap.image2,
            R.mipmap.image3,R.mipmap.image4,R.mipmap.image5,R.mipmap.image6, R.mipmap.image7,
            R.mipmap.image8,R.mipmap.image10,R.mipmap.image11,R.mipmap.image12,R.mipmap.cat_3};

    public ImageGalleryAdapter(Context context) {
        this.context = context;
    }

    public int getCount() { // 取得要显示内容的数量
        return images.length;
    }

    public Object getItem(int position) { // 每个资源的位置
        return images[position];
    }

    public long getItemId(int position) { // 取得每个项的ID
        return images[position];
    }

    // 将资源设置到一个组件之中，很明显这个组件是ImageView
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView iv = new ImageView(context);
        iv.setBackgroundColor(0xFFFFFFFF);
        iv.setImageResource(images[position]);// 给ImageView设置资源
        iv.setScaleType(ImageView.ScaleType.CENTER);// 设置对齐方式
/*        iv.setLayoutParams(new Gallery.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));*/
        int width = context.getResources().getDisplayMetrics().widthPixels;
        int height= context.getResources().getDisplayMetrics().heightPixels;
        iv.setLayoutParams(new Gallery.LayoutParams((int)(width*0.8f), height));
        return iv;
    }
}
