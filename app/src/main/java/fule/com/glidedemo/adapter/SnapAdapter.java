package fule.com.glidedemo.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import fule.com.glidedemo.R;
import fule.com.glidedemo.model.FeatureModel;

/**
 * 作者： njb
 * 时间： 2018/10/19 0019-下午 4:48
 * 描述： 图片滑动Adapter
 * 来源：
 */
public class SnapAdapter extends RecyclerView.Adapter<SnapAdapter.ViewHolder> {
    private List<FeatureModel> mList;
    Context context;

    public SnapAdapter(Context context, List<FeatureModel> mList) {
        this.context = context;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_special_offer, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        FeatureModel model = mList.get(position);
        holder.timeTv.setText(model.getTime());
    }

    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        private TextView timeTv;
        private ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv_shop_img);
            timeTv = itemView.findViewById(R.id.tv_bug_time);
        }
    }
}
