package fule.com.glidedemo.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import fule.com.glidedemo.R;

/**
 * 作者： njb
 * 时间： 2018/10/19 0019-上午 11:21
 * 描述： gallery适配器
 * 来源：
 */
public class MyGalleryAdapter extends BaseAdapter {
    private Context context;
    private Integer[] images = { R.mipmap.image1, R.mipmap.image2,
            R.mipmap.image3,R.mipmap.image4,R.mipmap.image5,R.mipmap.image6, R.mipmap.image7,
            R.mipmap.image8,R.mipmap.image10,R.mipmap.image11,R.mipmap.image12,R.mipmap.cat_3};

    public MyGalleryAdapter(Context c) {
        context = c;
    }

    public int getCount() {
        return images.length;
    }

    public Object getItem(int position) {
        return images[position];
    }

    public long getItemId(int position) {
        return images[position];
    }

    @SuppressLint("ClickableViewAccessibility")
    public View getView(int position, View convertView, ViewGroup parent) {
        final ImageView imageview = new ImageView(context);
        //图片自适应大小
/*                imageview.setLayoutParams(new Gallery.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));*/
        //设置图片大小为屏幕宽高，其中宽度为0.8
        int width = context.getResources().getDisplayMetrics().widthPixels;
        int height= context.getResources().getDisplayMetrics().heightPixels;
        imageview.setLayoutParams(new Gallery.LayoutParams((int)(width*0.8f), height));
        //设置图片的缩放模式
        imageview.setScaleType(ImageView.ScaleType.FIT_CENTER);
        imageview.setImageResource(images[position]);

        final int pos = position;

        //触摸galley的image的时候
        imageview.setOnTouchListener(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                imageview.setScaleType(ImageView.ScaleType.CENTER);
                imageview.setImageResource(images[pos]);

                return false;
            }
        });
        return imageview;
    }
}
