package fule.com.glidedemo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.Toast;

import fule.com.glidedemo.R;
import fule.com.glidedemo.adapter.MyGalleryAdapter;
import fule.com.glidedemo.application.App;

/**
 * 作者： njb
 * 时间： 2018/10/19 0019-上午 11:08
 * 描述： Gallery实现图片滑动居中对齐
 * 来源：
 */
public class GalleryActivity extends BaseActivity {
    private Gallery myGallery;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_gallery);
        //初始化视图
        initView();
        //初始化Adapter
        initGalleryAdapter();
    }

    /**
     * 初始化视图
     */
    private void initView() {
        myGallery = findViewById(R.id.myGallery);
    }

    /**
     * 初始化Adapter
     */
    private void initGalleryAdapter() {
        setTitle("电影海报");
        MyGalleryAdapter galAdapter = new MyGalleryAdapter(App.getContext());
        myGallery.setAdapter(galAdapter);
        //点击监听
        myGallery.setOnItemClickListener(new OnItemClickListenerImpl());
    }

    private class OnItemClickListenerImpl implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            Toast.makeText(GalleryActivity.this, String.valueOf(position),
                    Toast.LENGTH_SHORT).show();
        }
    }

}
