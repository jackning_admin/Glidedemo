package fule.com.glidedemo.activity;

import android.animation.Keyframe;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PathMeasure;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;

import net.qiujuer.genius.blur.StackBlur;

import fule.com.glidedemo.R;
import fule.com.glidedemo.application.App;
import fule.com.glidedemo.util.ThemeUtils;
import fule.com.glidedemo.view.AniManager;
import fule.com.glidedemo.view.BadgeView;
import fule.com.glidedemo.view.GlideCircleTransform;
import jp.wasabeef.glide.transformations.BlurTransformation;

import static com.bumptech.glide.request.RequestOptions.bitmapTransform;
import static fule.com.glidedemo.util.BlurBitmapUtil.blurBitmap;

public class MainActivity extends BaseActivity {
    private ImageView roundIv, filletIv, blurIv, bordIv, imageView, images;
    private Toolbar toolbar;
    private Bitmap mBitmap;
    private BadgeView buyNumView;//购物车上的数量标签
    private ImageView buyImg;// 界面上跑的小图片
    private AniManager mAniManager;
    private int num;
    private ImageView car;//购物车图标
    private Button btn2,btn3,btn4;
    private ConstraintLayout rl;
    private TextView btn1;
    private PathMeasure mPathMeasure;
    /**
     * 贝塞尔曲线中间过程的点的坐标
     */
    private float[] mCurrentPosition = new float[2];
    private TextView count;
    private int i = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
        setImages();
        setListener();
        App.sContext = MainActivity.this;
        ThemeUtils.initStatusBarColor(MainActivity.this, ThemeUtils.getPrimaryDarkColor(MainActivity.this));
    }

    private void initView() {
        roundIv = findViewById(R.id.round_iv);
        filletIv = findViewById(R.id.fillet_iv);
        blurIv = findViewById(R.id.blur_iv);
        bordIv = findViewById(R.id.bord_iv);
        imageView = findViewById(R.id.image);
        images = findViewById(R.id.images);
        toolbar = findViewById(R.id.toolbar);
        mAniManager = new AniManager();//动画实例
        car = findViewById(R.id.car);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);

        buyNumView = new BadgeView(this, car);//把这个数字标签放在购物车图标上
        buyNumView.setBadgePosition(BadgeView.POSITION_TOP_RIGHT);//放在图标中心
        buyNumView.setTextColor(Color.WHITE);//数字颜色
        buyNumView.setBadgeBackgroundColor(Color.BLUE);//背景颜色
        buyNumView.setTextSize(9);//数字大小

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ObjectAnimator animator = tada(btn1);
                animator.setRepeatCount(ValueAnimator.INFINITE);
                animator.start();

                ObjectAnimator nopeAnimator = nope(car);
                nopeAnimator.setRepeatCount(ValueAnimator.INFINITE);
                nopeAnimator.start();
            }
        }, 2000);
    }

    public static ObjectAnimator tada(View view) {
        return tada(view, 1f);
    }

    private void setImages() {
        //加载圆形图片

        Glide.with(this).load(R.mipmap.image).apply(RequestOptions.circleCropTransform().diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)).into(roundIv);
        //加载圆角图片
        Glide.with(this).load(R.mipmap.bg_iv).apply(bitmapTransform(new RoundedCorners(15)).override(300, 300)).into(filletIv);

        //图片高斯模糊
        Glide.with(this).load(R.mipmap.image)
                .apply(bitmapTransform(new BlurTransformation(100))).into(blurIv);

        //加载带边框的图片
        Glide.with(this).load(R.mipmap.bg_iv)
                .apply(new RequestOptions().error(this.getResources().getDrawable(R.mipmap.bg_iv))
                        .placeholder(R.mipmap.bg_iv).centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                        .transform(new GlideCircleTransform(this, 2, Color.parseColor("#ccFF4081")))
                ).into(bordIv);

        //RenderScriprt实现高斯模糊
        Bitmap mBitmap1 = ((BitmapDrawable) getResources().getDrawable(R.mipmap.meizi)).getBitmap();
        Bitmap bitmap = blurBitmap(this, mBitmap1, 20);
        imageView.setImageBitmap(bitmap);

        //Bitmap JNI Native
        mBitmap = ((BitmapDrawable) getResources().getDrawable(R.mipmap.meizi)).getBitmap();
        Bitmap newBitmap = StackBlur.blurNatively(mBitmap, 15, false);
        images.setImageBitmap(newBitmap);

        // Java
        Bitmap mBitmaps = StackBlur.blur(mBitmap, 18, false);
        images.setImageBitmap(mBitmaps);
    }

    private void setListener() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SettingActivity.class));
            }
        });
        bordIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, SnapRecyclerViewActivity.class));
            }
        });
        images.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this,GalleryActivity.class));
            }
        });
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               startActivity(new Intent(MainActivity.this,Gallery3DActivity.class));
            }
        });
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAnim(v);
            }
        });
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAnim(v);
            }
        });
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAnim(v);
            }
        });
        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startAnim(v);
               // startAnimation();
                startShakeByPropertyAnim(imageView, 0.9f, 1.1f, 10f, 1000);
                //start();
            }
        });
    }

    private void startAnimation() {
        //旋转动画
        final RotateAnimation animation = new RotateAnimation(0f, 360f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                0.5f);
        animation.setDuration(50);// 设置动画持续时间
        //位移动画
        TranslateAnimation animation2 = new TranslateAnimation(
                Animation.RELATIVE_TO_SELF, 0f, Animation.RELATIVE_TO_PARENT,
                1.0f, Animation.RELATIVE_TO_PARENT, 0f,
                Animation.RELATIVE_TO_PARENT, 1f);
        //缩放动画
        final ScaleAnimation animation3 =new ScaleAnimation(1f, 0f, 1f, 0f,
                Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation3.setDuration(3000);
        animation2.setDuration(3000);
        /** 常用方法 */
        animation.setRepeatCount(50);// 设置重复次数
        // animation.setFillAfter(boolean);//动画执行完后是否停留在执行完的状态
        //动画集
        AnimationSet animationSet = new AnimationSet(false);
        animationSet.addAnimation(animation);
        animationSet.addAnimation(animation2);
        animationSet.addAnimation(animation3);
        animationSet.setRepeatCount(10);
        car.setAnimation(animationSet);
        /** 开始动画 */
        animationSet.startNow();
    }

    private void startShakeByPropertyAnim(View view, float scaleSmall, float scaleLarge, float shakeDegrees, long duration) {
        if (view == null) {
            return;
        }

        //先变小后变大
        PropertyValuesHolder scaleXValuesHolder = PropertyValuesHolder.ofKeyframe(View.SCALE_X,
                Keyframe.ofFloat(0f, 1.0f),
                Keyframe.ofFloat(0.25f, scaleSmall),
                Keyframe.ofFloat(0.5f, scaleLarge),
                Keyframe.ofFloat(0.75f, scaleLarge),
                Keyframe.ofFloat(1.0f, 1.0f)
        );
        PropertyValuesHolder scaleYValuesHolder = PropertyValuesHolder.ofKeyframe(View.SCALE_Y,
                Keyframe.ofFloat(0f, 1.0f),
                Keyframe.ofFloat(0.25f, scaleSmall),
                Keyframe.ofFloat(0.5f, scaleLarge),
                Keyframe.ofFloat(0.75f, scaleLarge),
                Keyframe.ofFloat(1.0f, 1.0f)
        );

        //先往左再往右
        PropertyValuesHolder rotateValuesHolder = PropertyValuesHolder.ofKeyframe(View.ROTATION,
                Keyframe.ofFloat(0f, 0f),
                Keyframe.ofFloat(0.1f, -shakeDegrees),
                Keyframe.ofFloat(0.2f, shakeDegrees),
                Keyframe.ofFloat(0.3f, -shakeDegrees),
                Keyframe.ofFloat(0.4f, shakeDegrees),
                Keyframe.ofFloat(0.5f, -shakeDegrees),
                Keyframe.ofFloat(0.6f, shakeDegrees),
                Keyframe.ofFloat(0.7f, -shakeDegrees),
                Keyframe.ofFloat(0.8f, shakeDegrees),
                Keyframe.ofFloat(0.9f, -shakeDegrees),
                Keyframe.ofFloat(1.0f, 0f)
        );

        ObjectAnimator objectAnimator = ObjectAnimator.ofPropertyValuesHolder(view, scaleXValuesHolder, scaleYValuesHolder, rotateValuesHolder);
        objectAnimator.setDuration(duration);
        objectAnimator.start();
    }


    private void start(){

        Animation animation = AnimationUtils.loadAnimation(MainActivity.this, R.anim.rotate);
        car.startAnimation(animation);
    }

    //启动动画
    public void startAnim(View v) {
        int[] end_location = new int[2];
        int[] start_location = new int[2];
        v.getLocationInWindow(start_location);// 获取购买按钮的在屏幕的X、Y坐标（动画开始的坐标）
       car.getLocationInWindow(end_location);// 这是用来存储动画结束位置，也就是购物车图标的X、Y坐标
        buyImg = new ImageView(this);// buyImg是动画的图片
        buyImg.setImageResource(R.mipmap.sign);// 设置buyImg的图片

        //        mAniManager.setTime(5500);//自定义时间
        mAniManager.setAnim(this, buyImg, start_location, end_location);// 开始执行动画

        mAniManager.setOnAnimListener(new AniManager.AnimListener() {
            @Override
            public void setAnimBegin(AniManager a) {
                //动画开始时的监听
            }

            @SuppressLint("SetTextI18n")
            @Override
            public void setAnimEnd(AniManager a) {
                //动画结束后的监听
                num += 5;
                buyNumView.setText(num + "");
                buyNumView.show();
            }
        });
    }

    public static ObjectAnimator tada(View view, float shakeFactor) {

        PropertyValuesHolder pvhScaleX = PropertyValuesHolder.ofKeyframe(View.SCALE_X,
                Keyframe.ofFloat(0f, 1f),
                Keyframe.ofFloat(.1f, .9f),
                Keyframe.ofFloat(.2f, .9f),
                Keyframe.ofFloat(.3f, 1.1f),
                Keyframe.ofFloat(.4f, 1.1f),
                Keyframe.ofFloat(.5f, 1.1f),
                Keyframe.ofFloat(.6f, 1.1f),
                Keyframe.ofFloat(.7f, 1.1f),
                Keyframe.ofFloat(.8f, 1.1f),
                Keyframe.ofFloat(.9f, 1.1f),
                Keyframe.ofFloat(1f, 1f)
        );

        PropertyValuesHolder pvhScaleY = PropertyValuesHolder.ofKeyframe(View.SCALE_Y,
                Keyframe.ofFloat(0f, 1f),
                Keyframe.ofFloat(.1f, .9f),
                Keyframe.ofFloat(.2f, .9f),
                Keyframe.ofFloat(.3f, 1.1f),
                Keyframe.ofFloat(.4f, 1.1f),
                Keyframe.ofFloat(.5f, 1.1f),
                Keyframe.ofFloat(.6f, 1.1f),
                Keyframe.ofFloat(.7f, 1.1f),
                Keyframe.ofFloat(.8f, 1.1f),
                Keyframe.ofFloat(.9f, 1.1f),
                Keyframe.ofFloat(1f, 1f)
        );

        PropertyValuesHolder pvhRotate = PropertyValuesHolder.ofKeyframe(View.ROTATION,
                Keyframe.ofFloat(0f, 0f),
                Keyframe.ofFloat(.1f, -3f * shakeFactor),
                Keyframe.ofFloat(.2f, -3f * shakeFactor),
                Keyframe.ofFloat(.3f, 3f * shakeFactor),
                Keyframe.ofFloat(.4f, -3f * shakeFactor),
                Keyframe.ofFloat(.5f, 3f * shakeFactor),
                Keyframe.ofFloat(.6f, -3f * shakeFactor),
                Keyframe.ofFloat(.7f, 3f * shakeFactor),
                Keyframe.ofFloat(.8f, -3f * shakeFactor),
                Keyframe.ofFloat(.9f, 3f * shakeFactor),
                Keyframe.ofFloat(1f, 0)
        );

        return ObjectAnimator.ofPropertyValuesHolder(view, pvhScaleX, pvhScaleY, pvhRotate).
                setDuration(1000);
    }

    public static ObjectAnimator nope(View view) {
        int delta = view.getResources().getDimensionPixelOffset(R.dimen.spacing_medium);

        PropertyValuesHolder pvhTranslateX = PropertyValuesHolder.ofKeyframe(View.TRANSLATION_X,
                Keyframe.ofFloat(0f, 0),
                Keyframe.ofFloat(.10f, -delta),
                Keyframe.ofFloat(.26f, delta),
                Keyframe.ofFloat(.42f, -delta),
                Keyframe.ofFloat(.58f, delta),
                Keyframe.ofFloat(.74f, -delta),
                Keyframe.ofFloat(.90f, delta),
                Keyframe.ofFloat(1f, 0f)
        );

        return ObjectAnimator.ofPropertyValuesHolder(view, pvhTranslateX).
                setDuration(500);
    }




    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

}
