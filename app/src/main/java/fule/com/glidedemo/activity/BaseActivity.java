package fule.com.glidedemo.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;

import fule.com.glidedemo.R;

/**
 * 作者： njb
 * 时间： 2018/8/20 0020-下午 1:04
 * 描述： Activity基类
 * 来源：
 */
public class BaseActivity extends AppCompatActivity {


    private int theme;
    private SharedPreferences sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onPreCreate();
    }

    private void onPreCreate() {
        sp= PreferenceManager.getDefaultSharedPreferences(this);
        theme=sp.getInt("theme_change", R.style.Theme7);
        setTheme(theme);

    }

//    当Activity 回调onRestart时（从上一个页面返回），检查当前主题是否已将被更改。
    @Override
    protected void onRestart() {
        super.onRestart();
        int newTheme = sp.getInt("theme_change", theme);
        if (newTheme != theme) {
            recreate();
        }
    }

}
