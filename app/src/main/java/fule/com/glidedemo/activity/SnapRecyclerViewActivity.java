package fule.com.glidedemo.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.Gravity;

import com.github.rubensousa.gravitysnaphelper.GravitySnapHelper;

import java.util.ArrayList;
import java.util.List;

import fule.com.glidedemo.R;
import fule.com.glidedemo.adapter.SnapAdapter;
import fule.com.glidedemo.application.App;
import fule.com.glidedemo.model.FeatureModel;

/**
 * 作者： njb
 * 时间： 2018/10/19 0019-下午 5:15
 * 描述： 图片滑动居中的recyclerView
 * 来源：
 */
public class SnapRecyclerViewActivity extends BaseActivity {
    private RecyclerView rvStart,rvCenter,rvTop;
    private List<FeatureModel> modelList;
    private SnapAdapter snapAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_center_recyclerview);
        //初始化视图
        initView();
        //初始化数据
        initData();
        //初始化Adapter
        initSnaAdapter();
    }

    /**
     * 初始化视图
     */
    private void initView() {
        rvStart= findViewById(R.id.rv_start);
        rvCenter= findViewById(R.id.rv_center);
        rvTop= findViewById(R.id.rv_top);
    }

    /**
     * 初始化数据
     */
    private void initData() {
        modelList = new ArrayList<>();
        FeatureModel model = new FeatureModel();
        for (int i = 0; i <12; i++) {
            model.setImg("222");
            model.setTime("仅剩5天13小时30分42秒");
            modelList.add(model);
        }
    }

    /**
     * 初始化Adapter
     */
    private void initSnaAdapter() {
        snapAdapter = new SnapAdapter(App.getContext(),modelList);
        rvCenter.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        //设置滑动时居中对齐
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(rvCenter);
        rvCenter.setAdapter(snapAdapter);

        //设置滑动时在开始
        snapAdapter = new SnapAdapter(App.getContext(),modelList);
        rvStart.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        SnapHelper snapHelperStart = new GravitySnapHelper(Gravity.START);
        snapHelperStart.attachToRecyclerView(rvStart);
        rvStart.setAdapter(snapAdapter);

        //设置滑动时在末尾
        snapAdapter = new SnapAdapter(App.getContext(),modelList);
        rvTop.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.HORIZONTAL, false));
        SnapHelper snapHelperTop = new GravitySnapHelper(Gravity.END);
        snapHelperTop.attachToRecyclerView(rvTop);
        rvTop.setAdapter(snapAdapter);
    }

}
