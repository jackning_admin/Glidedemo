package fule.com.glidedemo.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

import fule.com.glidedemo.R;
import fule.com.glidedemo.adapter.ImageAdapter;
import fule.com.glidedemo.view.GalleryFlow;

/**
 * 作者： njb
 * 时间： 2018/10/19 0019-下午 2:48
 * 描述： 3d效果的Gallery
 * 来源：
 */
public class Gallery3DActivity extends BaseActivity {
    Integer[] images = { R.mipmap.image1, R.mipmap.image2,
            R.mipmap.image3,R.mipmap.image4,R.mipmap.image5,R.mipmap.image6, R.mipmap.image7,
            R.mipmap.image8,R.mipmap.image10,R.mipmap.image11,R.mipmap.image12,R.mipmap.cat_3};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_galleryflow);
        initView();
    }

    private void initView() {
        ImageAdapter adapter = new ImageAdapter(this, images);
        adapter.createReflectedImages();
        //创建倒影效果
        GalleryFlow galleryFlow = this.findViewById(R.id.Gallery);
        galleryFlow.setFadingEdgeLength(0);
        //图片之间的间距
        galleryFlow.setSpacing(-100);
        galleryFlow.setAdapter(adapter);
        galleryFlow.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(getApplicationContext(), String.valueOf(position), Toast.LENGTH_SHORT).show();
            }
        });
        galleryFlow.setSelection(4);
    }
}
