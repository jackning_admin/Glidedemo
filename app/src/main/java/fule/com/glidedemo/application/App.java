package fule.com.glidedemo.application;

import android.app.Application;
import android.content.Context;

/**
 * 作者： njb
 * 时间： 2018/8/20 0020-下午 1:04
 * 描述：
 * 来源：
 */
public class App extends Application {
    //全局Context
    public static Context sContext;

    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
    }


    public static Context getContext() {
        return sContext;
    }
}
