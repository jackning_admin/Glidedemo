package fule.com.glidedemo.model;

/**
 * 作者： njb
 * 时间： 2018/10/12 0012-下午 3:21
 * 描述： 精选
 * 来源：
 */
public class FeatureModel {
    private String title;
    private String name;
    private String img;
    private String time;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
